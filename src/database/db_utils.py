#!/usr/bin/python3.5

"""
This file defines a class having all the utilities needed for a database connection
The DatabaseHandler class defined in this file allows for
    # Database connection
    # Query execution with or without prepared statements
    # Matrix retrieval and iteration
It is used for retrieving the sensor's statistics stored in the local database
"""
import mysql.connector as connector
import ast
from mysql.connector.cursor import MySQLCursorPrepared
import numpy as np


class DatabaseHandler:
    dataset = []
    labels = []

    def __init__(self, host, user, password, db):
        """
        Initialize the internal state of the handler
        :param host: hostname used for the database connection
        :param user: username used for the database connection
        :param password: password used for the database connection
        :param db: schema name used
        """
        self.host = host
        self.user = user
        self.password = password
        self.db = db

    def init_connection(self):
        """
        Connects to the database with the internal settings
        then initializes the internal cursor
        """
        try:
            self.__db = connector.connect(host=self.host,
                                          user=self.user,
                                          passwd=self.password,
                                          db=self.db,
                                          use_pure=True)
            # Instantiate the cursor with support for prepared statements
            self.__prepared_cursor = self.__db.cursor(cursor_class=MySQLCursorPrepared)
            self.__cursor = self.__db.cursor()
        except connector.Error:
            print("Database connection error !")

    def execute_query(self, query):
        """
        Executes a query
        :param query: query to be executed
        """
        assert query is not None, 'Undefined query !'
        try:
            self.__cursor.execute(query)
        except connector.Error:
            print("SQL error please check your MySQL syntax !")

    def init_data(self):
        """
        Fetches the matrices and the labels from the last defined query
        This method just fills the internal arrays "dataset" and "labels"
        """
        for row in self.__cursor.fetchall():
            # Convert from a string list representation to an actual list
            matrix = ast.literal_eval(row[1])

            # Get the label
            label = row[2]

            # Convert to a numpy array
            matrix = np.array(matrix)

            self.dataset.append(matrix)
            self.labels.append(label)

        self.dataset = np.array(self.dataset)
        self.labels = np.array(self.labels)

    def execute_with_args(self, query, *args):
        """
        Execute a query using a prepared statement by replacing fieds with arguments passed to the function
        :param query: the query to be executed
        :param args: arguments to be added to the prepared fields of the query
        """
        try:
            if args:
                self.__prepared_cursor.execute(query, args)
                matrix = None
                row = self.__prepared_cursor.fetchone()
                if row is not None:
                    # Convert from a string list representation to an actual list
                    matrix = ast.literal_eval(row[1])

                    # Convert to a numpy array
                    matrix = np.array(matrix)
                    return matrix
                else:
                    return None

        except connector.Error:
            print("Error in args specification ! please check that you have the correct args !")

    def close_connection(self):
        """
        Close the connection to the database
        """
        self.__cursor.close()
