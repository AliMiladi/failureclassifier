#!/usr/bin/python3.5
"""
This file defines a class which provides a set of tools to manipulate data and plot it
"""
from matplotlib import pyplot as plt
import itertools
from random import shuffle

import keras
from keras import backend as K
from keras.backend import categorical_crossentropy
from keras.models import Sequential
from keras.layers import Conv2D, BatchNormalization, Activation, Dropout, MaxPooling2D, Flatten, Dense, Embedding, LSTM, \
    Conv1D, MaxPooling1D
from keras.optimizers import Adam
from numpy import array, vstack, hstack, amax, newaxis, arange, append, mean, std

import os

from sklearn.model_selection import RepeatedStratifiedKFold


class DataHandler:
    def __init__(self, dataset, train_ratio, train_valid_ratio):
        """
        Set initial state
        :param dataset: the dataset to operate on
        :param train_ratio: first splitting ratio
        :param train_valid_ratio: second splitting ratio
        """
        assert dataset is not None, 'Please provide a dataset !'
        assert train_ratio is not None, 'Please provide a training ratio !'
        assert train_valid_ratio is not None, 'Please provide a test ratio or train+valid ratio !'
        self.__dataset = dataset
        self.__train_ratio = train_ratio
        self.__train_valid_ratio = train_valid_ratio
        self.__plots_dirname = "plots"

    @staticmethod
    def save(path, ext='png', close=False, verbose=True):
        """
        Adapted from : https://gist.github.com/jhamrick/5320734
        Save a figure from pyplot.
        Parameters
        ----------
        path : string
            The path (and filename, without the extension) to save the
            figure to.
        ext : string (default='png')
            The file extension. This must be supported by the active
            matplotlib backend (see matplotlib.backends module).  Most
            backends support 'png', 'pdf', 'ps', 'eps', and 'svg'.
        close : boolean (default=True)
            Whether to close the figure after saving.  If you want to save
            the figure multiple times (e.g., to multiple formats), you
            should NOT close it in between saves or you will have to
            re-plot it.
        verbose : boolean (default=True)
            Whether to print information about when and where the image
            has been saved.
        """

        # Extract the directory and filename from the given path
        directory = os.path.split(path)[0]
        filename = "%s.%s" % (os.path.split(path)[1], ext)
        if directory == '':
            directory = '.'

        # If the directory does not exist, create it
        if not os.path.exists(directory):
            os.makedirs(directory)

        # The final path to save to
        savepath = os.path.join(directory, filename)

        if verbose:
            print("Saving figure to '%s'..." % savepath),

        # Actually save the figure
        plt.savefig(savepath)

        # Close it
        if close:
            plt.close()

        if verbose:
            print("Done")

    @staticmethod
    def get_max_value(dataset):
        """
        Simply computes and returns the maximal value of the given dataset
        :param dataset: expects a dataset of the shape [data, labels]
        """
        return amax(dataset[0])

    def make_test_data(self, dataset):
        """
        Initializes the test data and labels and shrinks the original dataset to let only the ratio given in parameter
        :param dataset: dataset to extract the test data from
        :return: test data and labels and the rest of the dataset
        """
        assert dataset is not None, 'Please provide a dataset !'
        # Retrieve the splits
        splits = self.__get_splits(dataset=dataset, ratio=self.__train_valid_ratio)
        data_classes = []
        labels_classes = []
        for i in range(len(splits[:, 0])):
            data_classes.append(splits[:, 0][:][i][0])
            labels_classes.append(splits[:, 0][:][i][1])

        data_classes = array(data_classes)
        labels_classes = array(labels_classes)

        x_test_to_stack = []
        data_to_stack = []
        y_test_to_stack = []
        labels_to_stack = []

        for i in range(len(splits[:, 0])):
            x_test_to_stack.append(data_classes[i][:splits[i, 1]])
            data_to_stack.append(data_classes[i][splits[i, 1]:])
            y_test_to_stack.append(labels_classes[i][:splits[i, 1]])
            labels_to_stack.append(labels_classes[i][splits[i, 1]:])

        x_test = vstack(x_test_to_stack)
        dataset[0] = vstack(data_to_stack)
        y_test = hstack(y_test_to_stack)
        dataset[1] = hstack(labels_to_stack)

        return x_test, y_test, dataset

    @staticmethod
    def __get_data_and_labels_for_class(dataset, label):
        data_for_class = []
        labels_for_class = []

        data = dataset[0]
        labels = dataset[1]

        # Add data in the corresponding list according to it's label
        for i in range(data.shape[0]):
            if labels[i] == label:
                data_for_class.append(data[i])
                labels_for_class.append(labels[i])
        data_for_class = array(data_for_class)
        labels_for_class = array(labels_for_class)
        return data_for_class, labels_for_class

    @staticmethod
    def normalize_dataset(dataset, less_than_one=True):
        """
        First computes the mean value of each "flow durations" feature according to the number of packets sent in a
        flow, then computes the mean value of each "response codes" feature Finally normalizes the training and
        validation data and labels according to a maximal value
        :param less_than_one: this parameter is by default set to true and specifies that the data in the dataset should
        be brought to the interval [0,1]. If this parameter is set to false, only means of the columns corresponding to
        flow durations sum and response codes sum are computed.
        :param dataset: if the parameter less_than_one is specified, the returned dataset is [data normalized between
        zero and one, labels] else the returned dataset is [data normalized with only means computed on columns
        flow_durations and response codes, labels].

        Note that the data has type numpy.float64 if the parameter less_than_one is specified, else the type numpy.int64
        is used for the data
        """
        if less_than_one:
            data = dataset[0].astype('float64')
        else:
            data = dataset[0]
        max_1 = amax(data, axis=0)
        max_ = amax(max_1, axis=0)
        for mat in data:
            for i in range(mat.shape[0]):
                if mat[i, 2] != 0:
                    mat[i, 4] /= mat[i, 2]  # flow durations sum
                mat[i, 4] /= 1000000  # This cell corresponds to the flow durations. It is represented in
                # microseconds which makes it very large compared to the other features that's the reason why we
                # bring it to seconds
                if mat[i, 1] != 0:
                    mat[i, 5] /= mat[i, 1]  # This cell corresponds to the sum of response codes. Here we compute the
                    #  mean according to the number of responses
                if less_than_one:
                    for j in range(mat.shape[1]):
                        if max_[j] != 0:
                            mat[i, j] /= max_[j]
        return [data, dataset[1]]

    @staticmethod
    def normalize_observation(dataset, mat, less_than_one=True):
        """
        Normalize a single matrix according to the given dataset
        :param dataset: reference dataset
        :param mat: matrix to normalize
        :param less_than_one: If set to true, the normalisation brings all the numbers of the matrix between 0 and 1
        else just compute some means
        :return: the normalized matrix
        """
        dataset = vstack([dataset, mat[newaxis, :]])
        if less_than_one:
            data = dataset.astype('float64')
            mat = mat.astype('float64')
        else:
            data = dataset

        max_1 = amax(data, axis=0)
        max_ = amax(max_1, axis=0)

        for i in range(mat.shape[0]):
            if mat[i, 2] != 0:
                mat[i, 4] /= mat[i, 2]  # flow durations sum
            mat[i, 4] /= 1000000  # This cell corresponds to the flow durations. It is represented in
            # microseconds which makes it very large compared to the other features that's the reason why we
            # bring it to seconds
            if mat[i, 0] != 0:
                mat[i, 5] /= mat[i, 0]  # This cell corresponds to the sum of response codes. Here we compute the
                #  mean according to the number of responses
            if less_than_one:
                for j in range(mat.shape[1]):
                    if max_[j] != 0:
                        mat[i, j] /= max_[j]
                        mat[i, j] /= max_[j]
        if less_than_one:
            return mat
        else:
            return mat.astype('int64')

    def split(self, dataset):
        """
        Splits the provided dataset with the given ratio
        :param dataset: The dataset to split. Should have the shape [array(data), array(labels)]
        :return training and validation sets and the associated labels
        """
        # Retrieve the splits
        splits = self.__get_splits(dataset=dataset, ratio=self.__train_ratio)
        data_classes = []
        labels_classes = []
        for i in range(len(splits[:, 0])):
            data_classes.append(splits[:, 0][:][i][0])
            labels_classes.append(splits[:, 0][:][i][1])

        data_classes = array(data_classes)
        labels_classes = array(labels_classes)

        x_train_to_stack = []
        x_valid_to_stack = []
        y_train_to_stack = []
        y_valid_to_stack = []

        for i in range(len(splits[:, 0])):
            x_train_to_stack.append(data_classes[i][:splits[i, 1]])
            x_valid_to_stack.append(data_classes[i][splits[i, 1]:])
            y_train_to_stack.append(labels_classes[i][:splits[i, 1]])
            y_valid_to_stack.append(labels_classes[i][splits[i, 1]:])

        # Build the train and validation sets
        x_train = vstack(x_train_to_stack)
        x_valid = vstack(x_valid_to_stack)
        y_train = hstack(y_train_to_stack)
        y_valid = hstack(y_valid_to_stack)

        return x_train, y_train, x_valid, y_valid

    def __get_splits(self, dataset, ratio):
        """
        Retrieve the necessary information to split the given dataset into data and labels with a ratio equals to the
        provided one
        :param dataset: The dataset to split. Needs to have the shape [[data],[labels]]
        :param ratio: The ratio of the biggest part
        :return: An array of arrays. Each array of those is made of an array containing the data and labels of each
        label and the splitting index corresponding to these data and labels.
        For example if we have data for labels 0 and 1, this method will return
            [
             [[data_class_0,labels_class_0],splitting_index_0],
             [[data_class_1,labels_class_1],splitting_index_1]
            ]
        """
        # Store the splits here
        classes_and_labels = []

        data = dataset[0]
        labels = dataset[1]

        # Data and labels should have the same number of elements otherwise the dataset is incorrect
        assert data.shape[0] == labels.shape[0], 'Data and labels does not have the same size !\nIncorrect dataset !'

        # Seen labels to not repeat the operations a bunch of times while the label has already been processed
        seen = []

        for i in range(len(labels)):
            if labels[i] not in seen:
                # Now the label has been already seen and processed
                seen.append(labels[i])

                # Get the data and labels corresponding to the given label
                class_i, label_i = self.__get_data_and_labels_for_class(dataset=dataset, label=labels[i])

                # Random shuffle
                shuffle(class_i)

                # Compute the stopping index for each array
                splitting_index_i = int(round(class_i.shape[0] * ratio))

                # Append to the resulting array
                classes_and_labels.append([[class_i, label_i], splitting_index_i])
        return array(classes_and_labels)

    def plot_confusion_matrix(self, cm, classes,
                              normalize=False,
                              title=None,
                              cmap=plt.cm.Blues,
                              verbose=False,
                              show=False,
                              save=False):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, newaxis]
            if verbose:
                print("Normalized confusion matrix")
        else:
            if verbose:
                print('Confusion matrix, without normalization')

        if verbose:
            print(cm)

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        if save:
            self.save("{dir}/{name}-confmat".format(dir=self.__plots_dirname, name=title))
        if show:
            plt.show()

    @staticmethod
    def encode_sequences(dataset):
        """
        Flattens the matrices in the dataset into sequences of numbers. Each matrix is flattened in a sequence of
        numbers which finally gives a list of sequences
        :param dataset: dataset to transform
        :return: sequences
        """
        seqs = []
        for mat in dataset:
            intermediate = []
            for i in range(mat.shape[0]):
                for j in range(mat.shape[1]):
                    intermediate.append(mat[i, j])
            seqs.append(intermediate)
        return array(seqs)

    def plot_metrics(self, metrics, legend, history, title=None, show=False, save=False):
        from matplotlib.ticker import MaxNLocator

        ax = plt.figure().gca()
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

        for metric in metrics:
            ax.plot(history.history[metric])

        plt.xlabel("epoch")
        plt.ylabel("percent")
        plt.legend(legend)
        plt.title(title)
        if save:
            self.save("{dir}/{name}-metrics".format(dir=self.__plots_dirname, name=title))
        if show:
            plt.show()

    @staticmethod
    def __reset_weights(model, lstm=False):
        session = K.get_session()
        for layer in model.layers:
            if getattr(layer, 'name') == "LSTM" or getattr(layer, 'name') == "LSTM_1":
                pass
            elif hasattr(layer, 'kernel_initializer'):
                layer.kernel.initializer.run(session=session)

    def benchmark(self, batches=None, learning_rates=None, metrics=None, legend=None, cnn=False, lstm=False, obj=None,
                  lrcn=False, title=None, show=False, save=False):
        assert metrics is not None, "Please provide a metrics list !"
        assert legend is not None, "Please a legend list according to the metrics you have given !"
        assert obj is not None, "Please provide a model CNN or LSTM !"
        n_metrics = len(metrics)
        model = Sequential()

        if cnn:
            model.add(Conv2D(32, kernel_size=(2, 2),
                             input_shape=obj.input_shape,
                             kernel_initializer="random_uniform"))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.3))
            model.add(MaxPooling2D(pool_size=(3, 3)))
            model.add(Dropout(0.25))
            model.add(Flatten())
            model.add(Dense(128, activation='relu',
                            kernel_initializer="random_uniform"))
            model.add(Dropout(0.5))
            model.add(Dense(obj.num_classes, activation='softmax', kernel_initializer="random_uniform"))
            model.compile(loss=keras.losses.categorical_crossentropy,
                          optimizer=keras.optimizers.Adam(lr=obj.learning_rate),
                          metrics=['accuracy'])

            if batches is None and learning_rates is None:
                print("Please specify a list of features to benchmark !")
                return
            elif batches is not None and learning_rates is not None:
                print("Please specify only one list of features to benchmark !")
            elif batches is not None:
                history = [[None for y in range(n_metrics)] for x in range(len(batches))]
                for i_bat, bat in enumerate(batches):
                    print("Testing batch size ", bat, " ...")

                    # First reset the weights
                    self.__reset_weights(model)

                    history[i_bat] = model.fit(obj.x_train, obj.y_train,
                                               batch_size=bat,
                                               epochs=obj.epochs,
                                               verbose=0,
                                               validation_data=(obj.x_valid, obj.y_valid))
                plt.figure(figsize=(15, 4))
                for n in arange(len(history)):
                    plt.subplot(1, len(history), n + 1)
                    for metric in metrics:
                        plt.plot(history[n].history[metric])
                    plt.ylim(0, 2)
                    plt.xlabel('Epochs')
                    plt.ylabel('Metrics')
                    plt.title("Batch size {size}".format(size=batches[n]))
                    plt.grid()
                plt.legend(legend)
                plt.suptitle("{title} Metrics ".format(title=title))
                plt.tight_layout()
                if save:
                    self.save("{dir}/{name}-batch-benchmarks".format(dir=self.__plots_dirname, name=title))
                if show:
                    plt.show()

            elif learning_rates is not None:
                history = [[None for y in range(n_metrics)] for x in range(len(learning_rates))]
                for i_lr, lr in enumerate(learning_rates):
                    print("Testing learning rate ", lr, " ...")

                    # First reset the weights
                    self.__reset_weights(model)

                    model.compile(loss=keras.losses.categorical_crossentropy,
                                  optimizer=keras.optimizers.Adam(lr=lr),
                                  metrics=['accuracy'])

                    history[i_lr] = model.fit(obj.x_train, obj.y_train,
                                              batch_size=obj.batch_size,
                                              epochs=obj.epochs,
                                              verbose=0,
                                              validation_data=(obj.x_valid, obj.y_valid))
                plt.figure(figsize=(15, 4))
                for n in arange(len(history)):
                    plt.subplot(1, len(history), n + 1)
                    for metric in metrics:
                        plt.plot(history[n].history[metric])
                    plt.ylim(0, 2)
                    plt.xlabel('Epochs')
                    plt.ylabel('Metrics')
                    plt.title("Learning rate {rate}".format(rate=learning_rates[n]))
                    plt.grid()
                plt.legend(legend)
                plt.suptitle("{title} Metrics ".format(title=title))
                plt.tight_layout()
                if save:
                    self.save("{dir}/{name}-lr-benchmarks".format(dir=self.__plots_dirname, name=title))
                if show:
                    plt.show()

        elif lstm:
            model.add(Embedding(obj.dimension, obj.embedding_length, input_length=obj.input_length))
            model.add(Dropout(0.3))
            model.add(LSTM(obj.neurons, return_sequences=True, name="LSTM_1"))
            model.add(Dropout(0.3))
            model.add(LSTM(obj.neurons, name="LSTM"))
            model.add(Dropout(0.3))
            model.add(Dense(obj.num_classes))
            model.add(BatchNormalization())
            model.add(Activation('softmax'))

            model.compile(loss='categorical_crossentropy',
                          optimizer=Adam(lr=obj.learning_rate),
                          metrics=['accuracy'])
            if batches is None and learning_rates is None:
                print("Please specify a list of features to benchmark !")
                return
            elif batches is not None and learning_rates is not None:
                print("Please specify only one list of features to benchmark !")
            elif batches is not None:
                history = [[None for y in range(n_metrics)] for x in range(len(batches))]
                for i_bat, bat in enumerate(batches):
                    print("Testing batch size ", bat, " ...")

                    # First reset the weights
                    self.__reset_weights(model)

                    history[i_bat] = model.fit(obj.x_train, obj.y_train,
                                               batch_size=bat,
                                               epochs=obj.epochs,
                                               verbose=0,
                                               validation_data=(obj.x_valid, obj.y_valid))
                plt.figure(figsize=(15, 4))
                for n in arange(len(history)):
                    plt.subplot(1, len(history), n + 1)
                    for metric in metrics:
                        plt.plot(history[n].history[metric])
                    plt.ylim(0, 2)
                    plt.xlabel('Epochs')
                    plt.ylabel('Metrics')
                    plt.title("Batch size {size}".format(size=batches[n]))
                    plt.grid()
                plt.legend(legend)
                plt.suptitle("{title} Metrics ".format(title=title))
                plt.tight_layout()
                if save:
                    self.save("{dir}/{name}-batch-benchmarks".format(dir=self.__plots_dirname, name=title))
                if show:
                    plt.show()

            elif learning_rates is not None:
                history = [[None for y in range(n_metrics)] for x in range(len(learning_rates))]
                for i_lr, lr in enumerate(learning_rates):
                    print("Testing learning rate ", lr, " ...")

                    # First reset the weights
                    self.__reset_weights(model)

                    model.compile(loss=keras.losses.binary_crossentropy,
                                  optimizer=keras.optimizers.Adam(lr=lr),
                                  metrics=['accuracy'])

                    history[i_lr] = model.fit(obj.x_train, obj.y_train,
                                              batch_size=obj.batch_size,
                                              epochs=obj.epochs,
                                              verbose=0,
                                              validation_data=(obj.x_valid, obj.y_valid))
                plt.figure(figsize=(15, 4))
                for n in arange(len(history)):
                    plt.subplot(1, len(history), n + 1)
                    for metric in metrics:
                        plt.plot(history[n].history[metric])
                    plt.ylim(0, 2)
                    plt.xlabel('Epochs')
                    plt.ylabel('Metrics')
                    plt.title("Learning rate {rate}".format(rate=learning_rates[n]))
                    plt.grid()
                plt.legend(legend)
                plt.suptitle("{title} Metrics ".format(title=title))
                plt.tight_layout()
                if save:
                    self.save("{dir}/{name}-lr-benchmarks".format(dir=self.__plots_dirname, name=title))
                if show:
                    plt.show()

        elif lrcn:
            model.add(Embedding(obj.lstm.dimension, obj.embedding_length, input_length=obj.input_length))
            model.add(Conv1D(filters=32,
                             kernel_size=3))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.4))
            model.add(MaxPooling1D(pool_size=2))
            model.add(LSTM(obj.lstm_neurons, return_sequences=True, name="LSTM"))
            model.add(Dropout(0.5))
            model.add(LSTM(obj.lstm_neurons, name="LSTM_1"))
            model.add(Dropout(0.3))
            model.add(Dense(obj.num_classes))
            model.add(BatchNormalization())
            model.add(Activation('softmax'))

            model.compile(loss=categorical_crossentropy,
                          optimizer=Adam(lr=obj.lstm.learning_rate),
                          metrics=['accuracy'])
            if batches is None and learning_rates is None:
                print("Please specify a list of features to benchmark !")
                return
            elif batches is not None and learning_rates is not None:
                print("Please specify only one list of features to benchmark !")
            elif batches is not None:
                history = [[None for y in range(n_metrics)] for x in range(len(batches))]
                for i_bat, bat in enumerate(batches):
                    print("Testing batch size ", bat, " ...")

                    # First reset the weights
                    self.__reset_weights(model)

                    history[i_bat] = model.fit(obj.x_train, obj.y_train,
                                               batch_size=bat,
                                               epochs=obj.epochs,
                                               verbose=0,
                                               validation_data=(obj.x_valid, obj.y_valid))
                plt.figure(figsize=(15, 4))
                for n in arange(len(history)):
                    plt.subplot(1, len(history), n + 1)
                    for metric in metrics:
                        plt.plot(history[n].history[metric])
                    plt.ylim(0, 2)
                    plt.xlabel('Epochs')
                    plt.ylabel('Metrics')
                    plt.title("Batch size {size}".format(size=batches[n]))
                    plt.grid()
                plt.legend(legend)
                plt.suptitle("{title} Metrics ".format(title=title))
                plt.tight_layout()
                if save:
                    self.save("{dir}/{name}-batch-benchmarks".format(dir=self.__plots_dirname, name=title))
                if show:
                    plt.show()

            elif learning_rates is not None:
                history = [[None for y in range(n_metrics)] for x in range(len(learning_rates))]
                for i_lr, lr in enumerate(learning_rates):
                    print("Testing learning rate ", lr, " ...")

                    # First reset the weights
                    self.__reset_weights(model)

                    model.compile(loss=keras.losses.binary_crossentropy,
                                  optimizer=keras.optimizers.Adam(lr=lr),
                                  metrics=['accuracy'])

                    history[i_lr] = model.fit(obj.x_train, obj.y_train,
                                              batch_size=obj.batch_size,
                                              epochs=obj.epochs,
                                              verbose=0,
                                              validation_data=(obj.x_valid, obj.y_valid))
                plt.figure(figsize=(15, 4))
                for n in arange(len(history)):
                    plt.subplot(1, len(history), n + 1)
                    for metric in metrics:
                        plt.plot(history[n].history[metric])
                    plt.ylim(0, 2)
                    plt.xlabel('Epochs')
                    plt.ylabel('Metrics')
                    plt.title("Learning rate {rate}".format(rate=learning_rates[n]))
                    plt.grid()
                plt.legend(legend)
                plt.suptitle("{title} Metrics ".format(title=title))
                plt.tight_layout()
                if save:
                    self.save("{dir}/{name}-lr-benchmarks".format(dir=self.__plots_dirname, name=title))
                if show:
                    plt.show()

        else:
            print("Please specify only one true value for cnn or lstm and the corresponding model object")
            return

    @staticmethod
    def mat_to_seq(matrix):
        """
        Flattens a single matrix on a sequence and returns it
        :param matrix: the matrix to flatten
        :return: the sequence as a result of the matrix flattening
        """
        seq = []
        for i in range(matrix.shape[0]):
            for j in range(matrix.shape[1]):
                seq.append(matrix[i, j])
        return array(seq)

    def cross_validation(self, model, num_splits, num_repeats=5, verbose=False):
        """
        K-fold cross validation of a model
        :param model: model to test
        :param num_splits: number of splits of the dataset
        :param num_repeats: number of repeats
        :param verbose: display more details in the console when set
\        """
        print("{k}-fold cross validation for {name} repeated {n} times".format(k=num_splits, name=model.name(),
              n=num_repeats))
        kfold = RepeatedStratifiedKFold(n_splits=num_splits, n_repeats=num_repeats)
        cvscores = []
        for train_indices, test_indices in kfold.split(model.x_train, model.y_train.argmax(1)):
            # create model
            new_model = self.__recreate_and_compile_model(model)
            # Fit the model
            new_model.fit(model.x_train[train_indices], model.y_train[train_indices], epochs=model.epochs,
                          batch_size=model.batch_size, verbose=0)
            # evaluate the model
            scores = new_model.evaluate(model.x_train[test_indices], model.y_train[test_indices], verbose=0)
            if verbose:
                print("%s: %.2f%%" % (new_model.metrics_names[1], scores[1] * 100))
            cvscores.append(scores[1] * 100)
        print("%.2f%% (+/- %.2f%%)" % (mean(cvscores), std(cvscores)))

    @staticmethod
    def __recreate_and_compile_model(model):
        new_model = Sequential()
        if model.name() == "CNN":
            new_model.add(Conv2D(32, kernel_size=(2, 2),
                                 input_shape=model.input_shape,
                                 kernel_initializer="random_uniform"))
            new_model.add(BatchNormalization())
            new_model.add(Activation('relu'))
            new_model.add(Dropout(0.3))
            new_model.add(MaxPooling2D(pool_size=(3, 3)))
            new_model.add(Dropout(0.25))
            new_model.add(Flatten())
            new_model.add(Dense(128, activation='relu',
                                kernel_initializer="random_uniform"))
            new_model.add(Dropout(0.5))
            new_model.add(Dense(model.num_classes, activation='softmax', kernel_initializer="random_uniform"))

            new_model.compile(loss=keras.losses.categorical_crossentropy,
                              optimizer=keras.optimizers.Adam(lr=model.learning_rate),
                              metrics=['accuracy'])

        elif model.name() == "LSTM":
            new_model.add(Embedding(model.dimension, model.embedding_length, input_length=model.input_length))
            new_model.add(Dropout(0.3))
            new_model.add(LSTM(model.neurons, return_sequences=True, name="LSTM_1"))
            new_model.add(Dropout(0.3))
            new_model.add(LSTM(model.neurons, name="LSTM"))
            new_model.add(Dropout(0.3))
            new_model.add(Dense(model.num_classes))
            new_model.add(BatchNormalization())
            new_model.add(Activation('softmax'))

            new_model.compile(loss='categorical_crossentropy',
                              optimizer=Adam(lr=model.learning_rate),
                              metrics=['accuracy'])

        elif model.name() == "LRCN":
            new_model.add(Embedding(model.lstm.dimension, model.embedding_length, input_length=model.input_length))
            new_model.add(Conv1D(filters=32,
                                 kernel_size=3))
            new_model.add(BatchNormalization())
            new_model.add(Activation('relu'))
            new_model.add(Dropout(0.4))
            new_model.add(MaxPooling1D(pool_size=2))
            new_model.add(LSTM(model.lstm_neurons, return_sequences=True, name="LSTM"))
            new_model.add(Dropout(0.5))
            new_model.add(LSTM(model.lstm_neurons, name="LSTM_1"))
            new_model.add(Dropout(0.3))
            new_model.add(Dense(model.num_classes))
            new_model.add(BatchNormalization())
            new_model.add(Activation('softmax'))

            new_model.compile(loss=categorical_crossentropy,
                              optimizer=Adam(lr=model.lstm.learning_rate),
                              metrics=['accuracy'])

        else:
            print("Model type unknown ==> returning empty")
        return new_model

    def dataset(self):
        return self.__dataset
