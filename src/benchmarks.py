#!/usr/bin/python3
"""
Different benchmarks to test the models efficiency against the test data
The user can specify several arguments and options to change the behaviour of this script
type ./benchmarks.py -h to access the help menu
"""
from models.deep_learning import DeepLearning

from argparse import ArgumentParser

# Define the models
cnn, lstm, lrcn = None, None, None

# Predefine the batches and the learning rates
batches = [5, 10, 15, 20]
learning_rates = [0.01, 0.001, 0.0001, 0.00001]

# Define the arguments of the script
parser = ArgumentParser()

parser.add_argument("-a", "--all", help="benchmark all the models", action="store_true", dest='all_models')
parser.add_argument("--cnn", help="benchmark the CNN", action="store_true")
parser.add_argument("--lstm", help="benchmark the LSTM", action="store_true")
parser.add_argument("--lrcn", help="benchmark the LRCN", action="store_true")
parser.add_argument("-v", "--verbose", help="Increase verbosity", action="store_true")

parser.add_argument("--dbhostname", help="hostname for the db connection", action="store")

parser.add_argument("-b", "--batches", help="benchmark batches", action="store_true")
parser.add_argument("-r", "--lrates", help="benchmark learning rates", action="store_true")

parser.add_argument("-s", "--save", help="save images to the \'plots\' folder", action="store_true")
parser.add_argument("-d", "--display", help="display images on screen", action="store_true")

parser.add_argument("-c", "--kfcv", help="k-fold cross validation\nrequires at least the number of splits\n"
                                         "usage : -c N_SPLITS [N_REPEATS]", nargs="*",
                    action="store", type=int)

# Parse the arguments of the script
args = parser.parse_args()

# Check whether arguments are valid
if not (args.all_models or args.cnn or args.lstm or args.lrcn):
    parser.error("No models stated to benchmark")

if not (args.batches or args.lrates):
    if args.kfcv and len(args.kfcv) not in (1, 2):
        parser.error(
            "Specify at least the number of splits for k-fold cross-validation or both the number of splits and "
            "the number of repeats respectively")
    elif not args.kfcv:
        parser.error("No features stated to benchmark")

# Support for database hostname
dl = None
if args.dbhostname:
    dl = DeepLearning(labelled_dataset_name="t_dataset", db_hostname=args.dbhostname)
else:
    dl = DeepLearning(labelled_dataset_name="t_dataset")

# Enable verbosity
if args.verbose:
    dl.verbose(True)

# Save plots to the file system with this argument
if args.save:
    dl.save(True)

# Display plots in the screen with this option
if args.display:
    dl.display(True)

# Check for arguments combinations
if args.all_models:
    cnn = dl.cnn(num_classes=3)
    lstm = dl.lstm(num_classes=3)
    lrcn = dl.lrcn(num_classes=3)

    dl.performance_metrics(cnn)
    dl.confusion_matrix(cnn)

    dl.performance_metrics(lstm)
    dl.confusion_matrix(lstm)

    dl.performance_metrics(lrcn)
    dl.confusion_matrix(lrcn)

    del cnn
    del lstm
    del lrcn

    if args.batches:
        cnn = dl.cnn(num_classes=3, train=False)
        lstm = dl.lstm(num_classes=3, train=False)
        lrcn = dl.lrcn(num_classes=3, train=False)
        dl.benchmark(cnn, batches=batches)
        dl.benchmark(lstm, batches=batches)
        dl.benchmark(lrcn, batches=batches)

        del cnn
        del lstm
        del lrcn

    if args.lrates:
        cnn = dl.cnn(num_classes=3, train=False)
        lstm = dl.lstm(num_classes=3, train=False)
        lrcn = dl.lrcn(num_classes=3, train=False)
        dl.benchmark(cnn, learning_rates=learning_rates)
        dl.benchmark(lstm, learning_rates=learning_rates)
        dl.benchmark(lrcn, learning_rates=learning_rates)

        del cnn
        del lstm
        del lrcn

    if args.kfcv:
        cnn = dl.cnn(num_classes=3, train=False)
        lstm = dl.lstm(num_classes=3, train=False)
        lrcn = dl.lrcn(num_classes=3, train=False)

        if len(args.kfcv) == 1:
            dl.cross_validation(model=cnn, num_splits=args.kfcv[0])
            dl.cross_validation(model=lstm, num_splits=args.kfcv[0])
            dl.cross_validation(model=lrcn, num_splits=args.kfcv[0])
        elif len(args.kfcv) == 2:
            dl.cross_validation(model=cnn, num_splits=args.kfcv[0], num_repeats=args.kfcv[1])
            dl.cross_validation(model=lstm, num_splits=args.kfcv[0], num_repeats=args.kfcv[1])
            dl.cross_validation(model=lrcn, num_splits=args.kfcv[0], num_repeats=args.kfcv[1])

        del cnn
        del lstm
        del lrcn

else:
    if args.cnn:
        cnn = dl.cnn(num_classes=3, epochs=100, batch_size=5, learning_rate=0.0001)

        dl.performance_metrics(cnn)
        dl.confusion_matrix(cnn)

        del cnn

        if args.batches:
            cnn = dl.cnn(num_classes=3, train=False, epochs=100, learning_rate=.0001)
            dl.benchmark(cnn, batches=batches)
            del cnn

        if args.lrates:
            cnn = dl.cnn(num_classes=3, train=False, epochs=100, learning_rate=.0001)
            dl.benchmark(cnn, learning_rates=learning_rates)
            del cnn

        if args.kfcv:
            cnn = dl.cnn(num_classes=3, train=False)

            if len(args.kfcv) == 1:
                dl.cross_validation(model=cnn, num_splits=args.kfcv[0])
            elif len(args.kfcv) == 2:
                dl.cross_validation(model=cnn, num_splits=args.kfcv[0], num_repeats=args.kfcv[1])

            del cnn

    if args.lstm:
        lstm = dl.lstm(num_classes=3, epochs=100)

        dl.performance_metrics(lstm)
        dl.confusion_matrix(lstm)

        del lstm

        if args.batches:
            lstm = dl.lstm(num_classes=3, train=False, epochs=100)
            dl.benchmark(lstm, batches=batches)
            del lstm

        if args.lrates:
            lstm = dl.lstm(num_classes=3, train=False, epochs=100)
            dl.benchmark(lstm, learning_rates=learning_rates)
            del lstm

        if args.kfcv:
            lstm = dl.lstm(num_classes=3, train=False)

            if len(args.kfcv) == 1:
                dl.cross_validation(model=lstm, num_splits=args.kfcv[0])
            elif len(args.kfcv) == 2:
                dl.cross_validation(model=lstm, num_splits=args.kfcv[0], num_repeats=args.kfcv[1])

            del lstm

    if args.lrcn:
        lrcn = dl.lrcn(num_classes=3, epochs=100)

        dl.performance_metrics(lrcn)
        dl.confusion_matrix(lrcn)

        del lrcn

        if args.batches:
            lrcn = dl.lrcn(num_classes=3, train=False)
            dl.benchmark(lrcn, batches=batches)
            del lrcn

        if args.lrates:
            lrcn = dl.lrcn(num_classes=3, train=False)
            dl.benchmark(lrcn, learning_rates=learning_rates)
            del lrcn

        if args.kfcv:
            lrcn = dl.lrcn(num_classes=3, train=False)

            if len(args.kfcv) == 1:
                dl.cross_validation(model=lrcn, num_splits=args.kfcv[0])
            elif len(args.kfcv) == 2:
                dl.cross_validation(model=lrcn, num_splits=args.kfcv[0], num_repeats=args.kfcv[1])

            del lrcn
