#!/usr/bin/python3.5

import faulthandler

faulthandler.enable()

from argparse import ArgumentParser
from time import sleep

from models.deep_learning import DeepLearning

# This import has been added for pyinstaller to work

num_classes = 3

# Store the sampling in seconds
sampling = 0

# Define the arguments of the script
parser = ArgumentParser()

parser.add_argument("-a", "--all",
                    help="use all the models for classification\nUsage : -a N_CLASSES [EPOCHS] [BATCH_SIZE] "
                         "[LEARNING_RATE]",
                    nargs="*", action="store",
                    dest='all_models')

parser.add_argument("--cnn", help="classify using CNN \nUsage : --cnn N_CLASSES [EPOCHS] [BATCH_SIZE] [LEARNING_RATE]",
                    nargs="*", action="store")
parser.add_argument("--lstm",
                    help="classify using LSTM\nUsage : --lstm N_CLASSES [EPOCHS] [BATCH_SIZE] [LEARNING_RATE]",
                    nargs="*", action="store")
parser.add_argument("--lrcn",
                    help="classify using LRCN\nUsage : --lrcn N_CLASSES [EPOCHS] [BATCH_SIZE] [LEARNING_RATE]",
                    nargs="*", action="store")

parser.add_argument("--dbhostname", help="hostname for the db connection", action="store")

parser.add_argument("-m", "--minutes", help="The sampling time is given in minutes", type=int)
parser.add_argument("-s", "--seconds", help="The sampling time is given in seconds", type=int)
parser.add_argument("-ho", "--hours", help="The sampling time is given in hours", type=int)
parser.add_argument("-v", "--verbose", action="store_true", help="Increase verbosity")

# Parse the arguments of the script
args = parser.parse_args()

if not (args.hours or args.minutes or args.seconds):
    parser.error('No sampling time provided please provide one')

if (args.hours and args.minutes) or (args.hours and args.seconds) or (args.minutes and args.seconds) or (
        args.hours and args.minutes and args.seconds):
    parser.error("Provide sampling in only one unit")

if not (args.all_models or args.cnn or args.lstm or args.lrcn):
    parser.error("No models specified for classification")

if args.cnn and len(args.cnn) not in (1, 2, 3, 4):
    parser.error("Specify at least the number of classes of the cnn. \nUsage : --cnn N_CLASSES [EPOCHS] [BATCH_SIZE] "
                 "[LEARNING_RATE]")

elif args.lstm and len(args.lstm) not in (1, 2, 3, 4):
    parser.error("Specify at least the number of classes of the lstm. \nUsage : --lstm N_CLASSES [EPOCHS] [BATCH_SIZE] "
                 "[LEARNING_RATE]")

elif args.lrcn and len(args.lrcn) not in (1, 2, 3, 4):
    parser.error("Specify at least the number of classes of the lrcn. \nUsage : --lrcn N_CLASSES [EPOCHS] [BATCH_SIZE] "
                 "[LEARNING_RATE]")

elif args.all_models and len(args.all_models) not in (1, 2, 3, 4):
    parser.error("Specify at least the number of classes of the models. \nUsage : -a N_CLASSES [EPOCHS] [BATCH_SIZE] "
                 "[LEARNING_RATE]")

# Init sampling period
if args.hours:
    sampling = args.hours * 3600
elif args.minutes:
    sampling = args.minutes * 60
elif args.seconds:
    sampling = args.seconds

# Defining the models to be used for predictions
# Note: they are already trained

dl = None
if args.dbhostname:
    dl = DeepLearning(labelled_dataset_name="t_dataset", db_hostname=args.dbhostname)
else:
    dl = DeepLearning(labelled_dataset_name="t_dataset")

if args.verbose:
    dl.verbose(True)

# List of models
models = []

if args.all_models:
    cnn, lstm, lrcn = None, None, None

    if len(args.all_models) == 1:
        cnn = dl.cnn(num_classes=int(args.all_models[0]))
        lstm = dl.lstm(num_classes=int(args.all_models[0]))
        lrcn = dl.lrcn(num_classes=int(args.all_models[0]))

    elif len(args.all_models) == 2:
        cnn = dl.cnn(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]))
        lstm = dl.lstm(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]))
        lrcn = dl.lrcn(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]))

    elif len(args.all_models) == 3:
        cnn = dl.cnn(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]),
                     batch_size=int(args.all_models[2]))
        lstm = dl.lstm(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]),
                       batch_size=int(args.all_models[2]))
        lrcn = dl.lrcn(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]),
                       batch_size=int(args.all_models[2]))

    elif len(args.all_models) == 4:
        cnn = dl.cnn(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]),
                     batch_size=int(args.all_models[2]),
                     learning_rate=float(args.all_models[3]))
        lstm = dl.lstm(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]),
                       batch_size=int(args.all_models[2]),
                       learning_rate=float(args.all_models[3]))
        lrcn = dl.lrcn(num_classes=int(args.all_models[0]), epochs=int(args.all_models[1]),
                       batch_size=int(args.all_models[2]),
                       learning_rate=float(args.all_models[3]))

    # List of models
    models = [
        cnn,
        lstm,
        lrcn
    ]
else:
    if args.cnn:
        cnn = None
        if len(args.cnn) == 1:
            try:
                cnn = dl.cnn(num_classes=int(args.cnn[0]))
            except ValueError:
                parser.error("Int value required for field N_CLASSES")
        elif len(args.cnn) == 2:
            try:
                cnn = dl.cnn(num_classes=int(args.cnn[0]), epochs=int(args.cnn[1]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES and EPOCHS")
        elif len(args.cnn) == 3:
            try:
                cnn = dl.cnn(num_classes=int(args.cnn[0]), epochs=int(args.cnn[1]), batch_size=int(args.cnn[2]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES, EPOCHS and BATCH_SIZE")
        elif len(args.cnn) == 4:
            try:
                cnn = dl.cnn(num_classes=int(args.cnn[0]), epochs=int(args.cnn[1]), batch_size=int(args.cnn[2]),
                             learning_rate=float(args.cnn[3]))
            except ValueError:
                parser.error(
                    "Int value required for fields N_CLASSES, EPOCHS and BATCH_SIZE, float value for field "
                    "LEARNING_RATE")
        models.append(cnn)

    if args.lstm:
        lstm = None
        if len(args.lstm) == 1:
            try:
                lstm = dl.lstm(num_classes=int(args.lstm[0]))
            except ValueError:
                parser.error("Int value required for field N_CLASSES ")
        elif len(args.lstm) == 2:
            try:
                lstm = dl.lstm(num_classes=int(args.lstm[0]), epochs=int(args.lstm[1]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES and EPOCHS")
        elif len(args.lstm) == 3:
            try:
                lstm = dl.lstm(num_classes=int(args.lstm[0]), epochs=int(args.lstm[1]), batch_size=int(args.lstm[2]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES, EPOCHS and BATCH_SIZE")
        elif len(args.lstm) == 4:
            try:
                lstm = dl.lstm(num_classes=int(args.lstm[0]), epochs=int(args.lstm[1]), batch_size=int(args.lstm[2]),
                               learning_rate=float(args.lstm[3]))
            except ValueError:
                parser.error(
                    "Int value required for fields N_CLASSES, EPOCHS and BATCH_SIZE, float value for field "
                    "LEARNING_RATE")
        models.append(lstm)

    if args.lrcn:
        lrcn = None
        if len(args.lrcn) == 1:
            try:
                lrcn = dl.lrcn(num_classes=int(args.lrcn[0]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES")
        elif len(args.lrcn) == 2:
            try:
                lrcn = dl.lrcn(num_classes=int(args.lrcn[0]), epochs=int(args.lrcn[1]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES and EPOCHS ")
        elif len(args.lrcn) == 3:
            try:
                lrcn = dl.lrcn(num_classes=int(args.lrcn[0]), epochs=int(args.lrcn[1]), batch_size=int(args.lrcn[2]))
            except ValueError:
                parser.error("Int value required for fields N_CLASSES, EPOCHS and BATCH_SIZE")
        elif len(args.lrcn) == 4:
            try:
                lrcn = dl.lrcn(num_classes=int(args.lrcn[0]), epochs=int(args.lrcn[1]), batch_size=int(args.lrcn[2]),
                               learning_rate=float(args.lrcn[3]))
            except ValueError:
                parser.error(
                    "Int value required for fields N_CLASSES, EPOCHS and BATCH_SIZE, float value for field "
                    "LEARNING_RATE")
        models.append(lrcn)

# Init observation number
n_obs = 1

# Dictionary that maps labels with names
state = {0: "Normal traffic", 1: "Buggy Wibox", 2: "Not a Wibox Server"}

# Entering an infinite loop in which the module will wait for a specified amount of time (provided in argument to the
# script) After this, it will try to read the next live observation from the database and predict on it It there
# isn't a live observation available, it will wait and retry later
while True:

    print("\n-----------------------------------------------\n")
    print("Waiting for {s} seconds ... ".format(s=sampling))
    # Wait for the sampling period
    sleep(sampling)

    # List to store the predictions
    preds = []
    percentages = []

    # Get the predictions of the models
    for model in models:
        if model is not None:
            pred, per = dl.predict_on_one_observation(model, n_obs)
            if pred is None:
                print("No new observation ...")
                break
            else:
                preds.append(pred)
                percentages.append(per)

    # We got all the predictions
    if len(preds) == len(models):
        print("Found new observation !!! (number {n})".format(n=n_obs))
        # Increment the observation number
        n_obs += 1
        # Print them out
        for index, model in enumerate(models):
            print("\n{model} predicts \'{prediction}\' "
                  "with {0:.2f}% probability".format(percentages[index], model=model.name(),
                                                     prediction=state[preds[index]]))
