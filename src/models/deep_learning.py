#!/usr/bin/python3.5

"""
This file entroduces a class which acts as an abstraction layer to simply create and manage machine learning models.
The models are the ones defined in this project CNN, LST anr LRCN
"""
import faulthandler

faulthandler.enable()

from database.db_utils import DatabaseHandler
from models.cnn import CNN
from models.lrcn import LRCN
from models.lstm import LsTm
from numpy import newaxis


class DeepLearning:
    def __init__(self, labelled_dataset_name="t_dataset", unlabelled_dataset_name="t_unlabelled_dataset",
                 train_ratio=0.8, test_ratio=0.35, db_hostname="localhost"):
        """
        Set the initial state of this abstraction layer
        :param labelled_dataset_name: the name of the table which holds the labelled observations in the database
        :param unlabelled_dataset_name: the name of the table which holds the unlabelled observations in the database
        :param train_ratio: ratio used to split the dataset to train/validation and test
        :param test_ratio: ratio used to split the remaining dataset into training and validation sets
        :param db_hostname: hostname of the machine holding the database
        """

        self.__train_ratio = train_ratio
        self.__test_ratio = test_ratio

        self.__db = DatabaseHandler(db_hostname,
                                    "root",
                                    "ali",
                                    "stream_info_schema")

        print("Connection to the database ...")
        self.__db.init_connection()
        print("Ok ...")

        query = "SELECT * FROM {name}".format(name=labelled_dataset_name)

        self.__db.execute_query(query)

        self.__db.init_data()
        self.__dataset = [self.__db.dataset, self.__db.labels]

        # Internal query to get te next observation
        self.__next_obs_query = "SELECT * FROM {name} WHERE matrix_id = ?".format(name=unlabelled_dataset_name)

        # Control the verbosity
        self.__verbose = False

        self.__save = False
        self.__display = False

    def __del__(self):
        self.__db.close_connection()
        print("Database connection closed !")

    def cnn(self, mat_rows=6, mat_cols=7, num_classes=2, epochs=50, batch_size=10, learning_rate=0.0001, train=True):
        """
        Creates and returns a cnn
        :param mat_rows: number of matrix rows
        :param mat_cols: number of matrix columns
        :param num_classes: number of classes to detect
        :param epochs: number of epochs in training
        :param batch_size: size of the batch used in training an validation
        :param learning_rate: learning rate used when compiling the model
        :param train: if set to true, the CNN gets trained with the given params else, it is returned without any
        training
        :return: CNN object
        """
        if self.__verbose:
            print("\n\t------------------------ CNN ------------------------ \n")

        cnn = CNN(mat_rows=mat_rows,
                  mat_cols=mat_cols,
                  num_classes=num_classes,
                  epochs=epochs,
                  batch_size=batch_size,
                  learning_rate=learning_rate,
                  verbose=self.__verbose)

        cnn.load_data(dataset=self.__dataset, train_ratio=self.__train_ratio, test_ratio=self.__test_ratio)

        if train:
            cnn.train()

        if self.__verbose:
            print("\n\t------------------------------------------------ \n")
        return cnn

    def lstm(self, neurons=10, embedding_length=64, input_length=6 * 7, epochs=40, batch_size=10, learning_rate=0.0001,
             num_classes=2, train=True):
        """
        Creates a LSTM and returns it
        :param neurons: number of neurons in the LSTM layers of this model
        :param embedding_length: length of the sequences that the input gets converted to
        :param input_length: length of the input
        :param epochs: number of epochs
        :param batch_size: batch size
        :param learning_rate: learning rate
        :param num_classes: number of classes to detect
        :param train: if set to true, the model gets trained before it gets returned
        :return: a LSTM object
        """
        if self.__verbose:
            print("\n\t------------------------ LSTM ------------------------ \n")

        lstm = LsTm(neurons=neurons,
                    embedding_length=embedding_length,
                    input_length=input_length,
                    epochs=epochs,
                    batch_size=batch_size,
                    learning_rate=learning_rate,
                    verbose=self.__verbose,
                    num_classes=num_classes)
        lstm.load_data(dataset=self.__dataset, train_ratio=self.__train_ratio, test_ratio=self.__test_ratio)

        if train:
            lstm.train()

        if self.__verbose:
            print("\n\t------------------------------------------------ \n")
        return lstm

    def lrcn(self, num_classes=2, epochs=50, batch_size=10, learning_rate=0.0001, lstm_neurons=10, embedding_length=64,
             input_length=42, train=True):
        """
        Creates a LRCN and returns it

        :param num_classes: number of classes
        :param epochs: number of epochs
        :param batch_size: size used for training and validation
        :param learning_rate: learning rate used for compiling
        :param lstm_neurons: number of neurons in the LSTM layers
        :param embedding_length: length of the sequences that the input gets converted to
        :param input_length: length of the input
        :param train: if set to true the model gets trained first
        :return:
        """
        if self.__verbose:
            print("\n\t------------------------ LRCN ------------------------ \n")

        lrcn = LRCN(num_classes=num_classes,
                    epochs=epochs,
                    batch_size=batch_size,
                    learning_rate=learning_rate,
                    lstm_neurons=lstm_neurons,
                    embedding_length=embedding_length,
                    input_length=input_length,
                    verbose=self.__verbose)

        lrcn.load_data(dataset=self.__dataset, train_ratio=self.__train_ratio, test_ratio=self.__test_ratio)

        if train:
            lrcn.train()

        if self.__verbose:
            print("\n\t------------------------------------------------ \n")
        return lrcn

    def predict_on_one_observation(self, model, n_obs):
        """
        Make a prediction on one single observation
        :param model: model to predict with
        :param n_obs: the observation number in the database
        :return: the prediction
        """
        assert n_obs > 0, 'negative observation number !'

        # Check the type of the model
        lstm_or_lrcn = type(model) is LsTm or type(model) is LRCN

        # Execute query with a prepared statement
        mat = self.__db.execute_with_args(self.__next_obs_query, n_obs)

        if mat is not None:
            # Normalize the new observation
            mat = model.normalize_observation(mat)

            # Transform to sequence if the model is a lstm or lrcn
            if lstm_or_lrcn:
                mat = model.mat_to_seq(matrix=mat)

            # Expand dims of the matrix or sequence to fit with the expected shape
            mat = mat[newaxis, :]

            # Get the predictions array and the percentage
            predictions, percentage = model.predict(data=mat, batch_size=model.batch_size)

            return predictions[0], percentage
        else:
            return None, None

    def confusion_matrix(self, model):
        model.predict(model.x_test, model.batch_size)
        model.plot_confusion_matrix(classes=["Normal traffic", "Buggy Wibox", "Not a Wibox server"],
                                    save=self.__save,
                                    show=self.__display)

    def performance_metrics(self, model):
        model.plot_performance_metrics(metrics=["acc", "val_acc", "loss", "val_loss"],
                                       legend=["Accuracy", "Validation accuracy", "Loss", "Validation loss"],
                                       save=self.__save,
                                       show=self.__display)

    def benchmark(self, model, batches=None, learning_rates=None):
        model.benchmark(batches,
                        learning_rates,
                        metrics=["acc", "val_acc", "loss", "val_loss"],
                        legend=["Accuracy", "Validation accuracy", "Loss", "Validation loss"],
                        save=self.__save,
                        show=self.__display)

    def cross_validation(self, model, num_splits, num_repeats=5):
        model.cross_validation(num_splits=num_splits, num_repeats=num_repeats, verbose=self.__verbose)

    def verbose(self, value=False):
        self.__verbose = value

    def save(self, value=False):
        self.__save = value

    def display(self, value=False):
        self.__display = value
