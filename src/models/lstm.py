#!/usr/bin/python3.5
"""
This file contains a class representing an LSTM (Long Short-Term Memory) network.
This latter is being used for sequence classification of network traffic
"""
from matplotlib import pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense, LSTM, Activation, BatchNormalization, Dropout
from keras.layers.embeddings import Embedding
from keras.optimizers import Adam
from numpy import array
from sklearn.metrics import confusion_matrix
from util.data_manipulations import DataHandler


class LsTm:
    """
    This class represents an LSTM model for sequence classification. For this model, a sequence is a list of statistics
    computed over the time.
    """

    def __init__(self, neurons, embedding_length, input_length, epochs, batch_size, learning_rate, num_classes,
                 verbose=False):
        """
        Initialise an LSTM
        :param embedding_length: size of the vector space in which the words (stats) will be embedded
        :param input_length: length of the input sequences which corresponds to the number of stats per matrix (rows)
        """
        self.neurons = neurons
        self.embedding_length = embedding_length
        self.input_length = input_length
        self.dimension = 0
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.num_classes = num_classes

        self.x_test = []
        self.y_test = []

        self.x_train = []
        self.y_train = []

        self.x_valid = []
        self.y_valid = []

        # Control the verbosity
        self.__verbose = verbose

        self.__model = Sequential()

    def load_data(self, dataset, train_ratio, test_ratio):
        """
        Loads the data from the provided dataset and splits it according to the provided ratios
        :param train_ratio: the ratio of the data used for training
        :param test_ratio: the ratio of the data used for testing
        :param dataset: the dataset to load has the shape [data(matrices), labels]
        """
        self.__data = DataHandler(dataset=dataset, train_ratio=train_ratio, train_valid_ratio=1 - test_ratio)
        dataset = self.__data.normalize_dataset(dataset=dataset, less_than_one=False)
        self.dimension = self.__data.get_max_value(dataset) + 1
        self.x_test, self.y_test, rest = self.__data.make_test_data(dataset=dataset)
        self.x_train, self.y_train, self.x_valid, self.y_valid = self.__data.split(dataset=rest)

        # Encode matrices to sequences
        self.x_train = self.__data.encode_sequences(self.x_train)
        self.x_valid = self.__data.encode_sequences(self.x_valid)
        self.x_test = self.__data.encode_sequences(self.x_test)

        # convert class vectors to binary categories
        self.y_train = keras.utils.to_categorical(self.y_train, self.num_classes)
        self.y_valid = keras.utils.to_categorical(self.y_valid, self.num_classes)

        if self.__verbose:
            print('x_train shape:', self.x_train.shape)
            print(self.x_train.shape[0], 'train samples')
            print(self.x_valid.shape[0], 'validation samples')
            print(self.x_test.shape[0], 'test samples')

    def train(self):
        print("Training {name} ...".format(name=self.name()))
        self.__model.add(Embedding(self.dimension, self.embedding_length, input_length=self.input_length))
        self.__model.add(Dropout(0.3))
        self.__model.add(LSTM(self.neurons, return_sequences=True))
        self.__model.add(Dropout(0.3))
        self.__model.add(LSTM(self.neurons))
        self.__model.add(Dropout(0.3))
        self.__model.add(Dense(self.num_classes))
        self.__model.add(BatchNormalization())
        self.__model.add(Activation('softmax'))

        self.__model.compile(loss='categorical_crossentropy',
                             optimizer=Adam(lr=self.learning_rate),
                             metrics=['accuracy'])

        if self.__verbose:
            print(self.__model.summary())

            self.__history = self.__model.fit(self.x_train,
                                              self.y_train,
                                              epochs=self.epochs,
                                              batch_size=self.batch_size,
                                              validation_data=(self.x_valid, self.y_valid),
                                              verbose=1)
        else:
            self.__history = self.__model.fit(self.x_train,
                                              self.y_train,
                                              epochs=self.epochs,
                                              batch_size=self.batch_size,
                                              validation_data=(self.x_valid, self.y_valid),
                                              verbose=0)

    def add_layer(self, layer):
        self.__model.add(layer)

    def predict(self, data, batch_size):
        """
        Predicts on the given data and the batch size
        :param data: data to predict on
        :param batch_size: batch size
        :return: A numpy array with prediction percentages
        """
        self.__preds = self.__model.predict_classes(x=data, batch_size=batch_size)

        percentages = self.__model.predict(x=data, batch_size=batch_size)

        percentage = percentages.max() * 100

        return self.__preds, percentage

    def plot_confusion_matrix(self, classes,
                              normalize=False,
                              title='Confusion matrix',
                              cmap=plt.cm.Blues,
                              show=False,
                              save=False):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        plt.figure()
        cm = confusion_matrix(self.y_test, self.__preds)
        self.__data.plot_confusion_matrix(cm, classes, normalize, self.name(), cmap, verbose=self.__verbose,
                                          show=show,
                                          save=save)
        # plt.show()

    def plot_performance_metrics(self, metrics, legend,
                                 show=False,
                                 save=False):
        self.__data.plot_metrics(metrics=metrics, legend=legend, history=self.__history, title=self.name(),
                                 show=show,
                                 save=save)

    def benchmark(self, batches=None, learning_rates=None, metrics=None, legend=None,
                  show=False,
                  save=False):
        self.__data.benchmark(batches=batches,
                              learning_rates=learning_rates,
                              metrics=metrics,
                              legend=legend,
                              lstm=True,
                              obj=self,
                              title=self.name(),
                              show=show,
                              save=save)

    def data(self):
        return self.__data

    def mat_to_seq(self, matrix):
        """
        Makes a sequence out of a matrix and returns it
        :param matrix: The matrix to encode
        :return: a sequence as a result of the transformation of the matrix
        """
        return self.__data.mat_to_seq(matrix)

    def cross_validation(self, num_splits, num_repeats=5, verbose=False):
        self.__data.cross_validation(self, num_splits=num_splits, num_repeats=num_repeats, verbose=verbose)

    def evaluate(self, x=None, y=None, batch_size=None, verbose=False):
        _verbose = 0
        if verbose:
            _verbose = 1
        self.__model.evaluate(x, y, batch_size, verbose=_verbose)

    @staticmethod
    def name():
        return "LSTM"

    def normalize_observation(self, matrix):
        return self.__data.normalize_observation(dataset=array(self.__data.dataset()[0]), mat=array(matrix),
                                                 less_than_one=False)
