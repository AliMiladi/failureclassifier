#!/usr/bin/python3.5
"""
This file contains a class that represents an LRCN (Long-term Recurrent Convolutional Network) network.
This made of a CNN which layers have been distributed across the time followed by an LSTM.
"""

from matplotlib import pyplot as plt
from keras.backend import categorical_crossentropy
from keras.layers import BatchNormalization, Activation, Dropout, LSTM, Dense, Embedding, Conv1D, MaxPooling1D
from keras.models import Sequential
from keras.optimizers import Adam
from sklearn.metrics import confusion_matrix

from models.lstm import LsTm


class LRCN:
    def __init__(self,
                 num_classes,
                 epochs,
                 batch_size,
                 learning_rate,
                 lstm_neurons,
                 embedding_length,
                 input_length,
                 verbose=False):
        """
        Initialize the LRCN
        :param num_classes: number of classes to classify
        :param epochs: number of epochs
        :param batch_size: batch size
        :param learning_rate: learning rate
        :param lstm_neurons: number of neurons of the subsequent lstm
        """
        self.__model = Sequential()

        self.num_classes = num_classes
        self.epochs = epochs
        self.batch_size = batch_size
        self._learning_rate = learning_rate
        self.lstm_neurons = lstm_neurons
        self.embedding_length = embedding_length
        self.input_length = input_length

        self.lstm = LsTm(lstm_neurons, embedding_length, input_length, epochs, batch_size, learning_rate, num_classes,
                         verbose=verbose)

        self.x_train = []
        self.y_train = []
        self.x_test = []
        self.y_test = []
        self.x_valid = []
        self.y_valid = []

        # Control the verbosity
        self.__verbose = verbose

    def load_data(self, dataset, train_ratio, test_ratio):
        """
        Loads the data from the database internally. Uses LSTM's load_data function
        :param dataset: datase to load
        :param train_ratio: ratio for first splitting
        :param test_ratio: ratio for second splitting
        :return:
        """
        self.lstm.load_data(dataset, train_ratio, test_ratio)
        self.x_train, self.y_train, self.x_valid, self.y_valid, self.x_test, self.y_test = [self.lstm.x_train,
                                                                                            self.lstm.y_train,
                                                                                            self.lstm.x_valid,
                                                                                            self.lstm.y_valid,
                                                                                            self.lstm.x_test,
                                                                                            self.lstm.y_test]

    def train(self):
        """
        Train the model
        :return:
        """
        print("Training {n} ...".format(n=self.name()))
        self.__model.add(Embedding(self.lstm.dimension, self.embedding_length, input_length=self.input_length))
        self.__model.add(Conv1D(filters=32,
                                kernel_size=3))
        self.__model.add(BatchNormalization())
        self.__model.add(Activation('relu'))
        self.__model.add(Dropout(0.4))
        self.__model.add(MaxPooling1D(pool_size=2))
        self.__model.add(LSTM(self.lstm_neurons, return_sequences=True))
        self.__model.add(Dropout(0.5))
        self.__model.add(LSTM(self.lstm_neurons))
        self.__model.add(Dropout(0.3))
        self.__model.add(Dense(self.num_classes))
        self.__model.add(BatchNormalization())
        self.__model.add(Activation('softmax'))

        self.__model.compile(loss=categorical_crossentropy,
                             optimizer=Adam(lr=self.lstm.learning_rate),
                             metrics=['accuracy'])

        if self.__verbose:
            self.__model.summary()

            self.__history = self.__model.fit(self.x_train,
                                              self.y_train,
                                              epochs=self.epochs,
                                              batch_size=self.batch_size,
                                              validation_data=(self.x_valid, self.y_valid),
                                              verbose=1)
        else:
            self.__history = self.__model.fit(self.x_train,
                                              self.y_train,
                                              epochs=self.epochs,
                                              batch_size=self.batch_size,
                                              validation_data=(self.x_valid, self.y_valid),
                                              verbose=0)

    def predict(self, data, batch_size):
        """
        Predicts on the given data and the batch size
        :param data: data to predict on
        :param batch_size: batch size
        :return: A numpy array with prediction percentages
        """
        self.__preds = self.__model.predict_classes(x=data, batch_size=batch_size)

        percentages = self.__model.predict(x=data, batch_size=batch_size)

        percentage = percentages.max() * 100

        return self.__preds, percentage

    def plot_confusion_matrix(self, classes,
                              normalize=False,
                              title=None,
                              cmap=plt.cm.Blues,
                              show=False,
                              save=False):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        plt.figure()
        data = self.lstm.data()
        cm = confusion_matrix(self.y_test, self.__preds)
        data.plot_confusion_matrix(cm, classes, normalize, self.name(), cmap, verbose=self.__verbose, show=show,
                                   save=save)
        # plt.show()

    def plot_performance_metrics(self, metrics, legend,
                                 show=False,
                                 save=False):
        data = self.lstm.data()
        data.plot_metrics(metrics=metrics, legend=legend, history=self.__history, title=self.name(),
                          show=show,
                          save=save)

    def benchmark(self, batches=None, learning_rates=None, metrics=None, legend=None,
                  show=False,
                  save=False):
        data = self.lstm.data()
        data.benchmark(batches=batches,
                       learning_rates=learning_rates,
                       metrics=metrics,
                       legend=legend,
                       lrcn=True,
                       obj=self,
                       title=self.name(),
                       show=show,
                       save=save)

    def mat_to_seq(self, matrix):
        """
        Makes a sequence out of a matrix and returns it
        :param matrix: The matrix to encode
        :return: a sequence as a result of the transformation of the matrix
        """
        return self.lstm.mat_to_seq(matrix)

    def cross_validation(self, num_splits, num_repeats=5, verbose=False):
        self.lstm.data().cross_validation(self, num_splits=num_splits, num_repeats=num_repeats, verbose=verbose)

    def evaluate(self, x=None, y=None, batch_size=None, verbose=False):
        _verbose = 0
        if verbose:
            _verbose = 1
        self.__model.evaluate(x, y, batch_size, verbose=_verbose)

    @staticmethod
    def name():
        return "LRCN"

    def normalize_observation(self, matrix):
        return self.lstm.normalize_observation(matrix)
