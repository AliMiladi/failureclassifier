#!/usr/bin/python3.5
"""
This file contains a class representing a CNN (Convolutional Neural Network).
This model is meant to detect and classify images into categories. However, it is used to classify a sequences of
feature vectors disposed as a matrix.
"""

import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D, Activation, BatchNormalization
from keras import backend as K
from typing import Any, Tuple
from numpy import array

from sklearn.metrics import confusion_matrix

from util.data_manipulations import DataHandler


class CNN:
    input_shape = []  # type: Tuple[Any, Any, int]

    def __init__(self, mat_rows, mat_cols, num_classes, epochs=10, batch_size=2, learning_rate=0.0001, verbose=False):
        """
        Initialize the initial state ot the CNN
        :param mat_rows: number of rows per matrix in input
        :param mat_cols: number of columns per matrix in input
        :param num_classes: number of classes to treat
        :param epochs: number of epochs to use during the training
        :param batch_size: size of the batches to use while training and validating the model
        """
        self.__model = Sequential()

        self.num_classes = num_classes
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate

        self.mat_rows = mat_rows
        self.mat_cols = mat_cols

        self.x_train = []
        self.y_train = []

        self.x_valid = []
        self.y_valid = []

        self.x_test = []
        self.y_test = []

        # Control the verbosity
        self.__verbose = verbose

    def add_layer(self, layer):
        self.__model.add(layer)

    def load_data(self, dataset, train_ratio, test_ratio):
        """
        Initialises the training, validation and test sets according to the given ratios
        :param dataset: dataset to initialize from
        :param train_ratio: training ratio
        :param test_ratio: testing ratio
        """
        self.__data = DataHandler(dataset=dataset, train_ratio=train_ratio, train_valid_ratio=1 - test_ratio)
        dataset = self.__data.normalize_dataset(dataset=dataset)
        self.x_test, self.y_test, rest = self.__data.make_test_data(dataset=dataset)
        self.x_train, self.y_train, self.x_valid, self.y_valid = self.__data.split(dataset=rest)
        if K.image_data_format() == 'channels_first':
            self.x_train = self.x_train.reshape(self.x_train.shape[0], 1, self.mat_rows, self.mat_cols)
            self.x_valid = self.x_valid.reshape(self.x_valid.shape[0], 1, self.mat_rows, self.mat_cols)
            self.input_shape = (1, self.mat_rows, self.mat_cols)
        else:
            self.x_train = self.x_train.reshape(self.x_train.shape[0], self.mat_rows, self.mat_cols, 1)
            self.x_valid = self.x_valid.reshape(self.x_valid.shape[0], self.mat_rows, self.mat_cols, 1)
            self.input_shape = (self.mat_rows, self.mat_cols, 1)

        self.x_train = self.x_train.astype('float32')
        self.y_train = self.y_train.astype('float32')
        self.x_valid = self.x_valid.astype('float32')
        self.y_valid = self.y_valid.astype('float32')
        self.x_test = self.x_test.astype('float32')

        if self.__verbose:
            print('x_train shape:', self.x_train.shape)
            print(self.x_train.shape[0], 'train samples')
            print(self.x_valid.shape[0], 'validation samples')
            print(self.x_test.shape[0], 'test samples')

        # convert class vectors to binary categories
        self.y_train = keras.utils.to_categorical(self.y_train, self.num_classes)
        self.y_valid = keras.utils.to_categorical(self.y_valid, self.num_classes)

    def train(self):
        """
        Trains the model with the internal parameters
        When called, this method completely defines the model, compiles it and trains it.
        """
        print("Training {n} ...".format(n=self.name()))
        self.__model.add(Conv2D(32,
                                kernel_size=(2, 2),
                                input_shape=self.input_shape))
        self.__model.add(BatchNormalization())
        self.__model.add(Activation('relu'))
        self.__model.add(Dropout(0.3))
        self.__model.add(MaxPooling2D(pool_size=(3, 3)))
        self.__model.add(Dropout(0.25))
        self.__model.add(Flatten())

        self.__model.add(Dense(128, activation='relu'))
        self.__model.add(Dropout(0.5))
        self.__model.add(Dense(self.num_classes, activation='softmax'))

        self.__model.compile(loss=keras.losses.categorical_crossentropy,
                             optimizer=keras.optimizers.Adam(lr=self.learning_rate),
                             metrics=['accuracy'])

        if self.__verbose:
            # Print out a summary before the training
            self.__model.summary()

            self.__history = self.__model.fit(self.x_train, self.y_train,
                                              batch_size=self.batch_size,
                                              epochs=self.epochs,
                                              verbose=1,
                                              validation_data=(self.x_valid, self.y_valid))
        else:
            self.__history = self.__model.fit(self.x_train, self.y_train,
                                              batch_size=self.batch_size,
                                              epochs=self.epochs,
                                              verbose=0,
                                              validation_data=(self.x_valid, self.y_valid))

    def predict(self, data, batch_size):
        """
        Predicts on the given data and the batch size
        :param data: data to predict on
        :param batch_size: batch size
        :return: A numpy array with prediction percentages
        """
        if K.image_data_format() == 'channels_first':
            self.x_test = self.x_test.reshape(self.x_test.shape[0], 1, self.mat_rows, self.mat_cols)
            data = data.reshape(data.shape[0], 1, self.mat_rows, self.mat_cols)
        else:
            self.x_test = self.x_test.reshape(self.x_test.shape[0], self.mat_rows, self.mat_cols, 1)
            data = data.reshape(data.shape[0], self.mat_rows, self.mat_cols, 1)

        self.__preds = self.__model.predict_classes(x=data, batch_size=batch_size)

        percentages = self.__model.predict(x=data, batch_size=batch_size)

        percentage = percentages.max() * 100

        return self.__preds, percentage

    def plot_performance_metrics(self, metrics, legend,
                                 show=False,
                                 save=False):
        """
        Plots the given list of performance metrics given them the legend passed in argument
        :param metrics: list of metrics to be plotted
        :param legend: list of legends
        """
        self.__data.plot_metrics(metrics=metrics, legend=legend, history=self.__history, title=self.name(),
                                 show=show,
                                 save=save)

    def benchmark(self, batches=None, learning_rates=None, metrics=None, legend=None,
                  show=False,
                  save=False):
        """
        Abstracts the companion DataHandler object's benchmark method which provides way to benchmark models on their
        batch sizes or learning rates
        :param batches: array of batch sizes to test
        :param learning_rates: array of learning rates to test
        :param metrics: array of metric names to extract and plot
        :param legend: visible name of the metrics
        :param show: decides whether to display a plot to the screen or not
        :param save: decides whether to save the generated plots to the file system
        :return:
        """
        self.__data.benchmark(batches=batches,
                              learning_rates=learning_rates,
                              metrics=metrics,
                              legend=legend,
                              cnn=True,
                              obj=self,
                              title=self.name(),
                              show=show,
                              save=save)

    def plot_confusion_matrix(self, classes,
                              normalize=False,
                              title='Confusion matrix',
                              cmap=plt.cm.Blues,
                              show=False,
                              save=False):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        plt.figure()
        cm = confusion_matrix(self.y_test, self.__preds)
        self.__data.plot_confusion_matrix(cm, classes, normalize, self.name(), cmap, verbose=self.__verbose,
                                          show=show,
                                          save=save)

    def cross_validation(self, num_splits, num_repeats=5, verbose=False):
        """
        Shadows companion DataHandler's cross_validation method
        :param num_splits: number of splits of the dataset
        :param num_repeats: number of repeats
        :param verbose: activate verbose mode
        """
        self.__data.cross_validation(self, num_splits=num_splits,num_repeats=num_repeats, verbose=verbose)

    def evaluate(self, x=None, y=None, batch_size=None, verbose=False):
        """
        Computes the loss on some input data, batch by batch (from keras evaluate function).
        :param x: input data
        :param y: labels
        :param batch_size: batch size
        :param verbose: verbosity parameter
        """
        _verbose = 0
        if verbose:
            _verbose = 1
        self.__model.evaluate(x, y, batch_size, verbose=_verbose)

    @staticmethod
    def name():
        """
        :return: name of the current model
        """
        return "CNN"

    def normalize_observation(self, matrix):
        """
        Normalize a single observation according to a dataset
        :param matrix:
        :return:
        """
        return self.__data.normalize_observation(dataset=array(self.__data.dataset()[0]), mat=array(matrix))
