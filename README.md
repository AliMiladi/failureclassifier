# Failuire classifier machine learning module

## Abstract

This module has been developed in python using the machine learning technique `deep learning`.

For this purpose, a `Convolutional Neural Network (CNN)`, a `Long Short-Term Memory (LSTM)` and a `Long-term Recurrent Convolutional Network` models has been developed.

## Requirements

To install the dependencies please follow the steps above :

* Clone the repo

* Install python 3 dependencies (`sudo apt-get install python3-dev python3-tk`)

* Install project dependencies (`sudo pip3 install -r requirements.txt`)

* Make the entrypoints executable (`sudo chmod +x live_classify.py benchmarks.py`)

* Execute (`./live_classify.py -h`)

* Execute (`./benchmarks.py -h`)



